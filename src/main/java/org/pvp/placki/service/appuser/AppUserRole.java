package org.pvp.placki.service.appuser;

public enum AppUserRole {
    USER,
    ADMIN
}
