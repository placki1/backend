package org.pvp.placki.service.export;

import com.itextpdf.text.pdf.BaseFont;
import io.github.classgraph.Resource;
import lombok.SneakyThrows;
import org.pvp.placki.entity.recipe.Ingredients;
import org.pvp.placki.entity.recipe.Recipe;
import org.pvp.placki.entity.recipe.Steps;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PdfService {

    @SneakyThrows
    public static void constructPdf(Recipe recipe){
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(TemplateMode.HTML);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        Context context = new Context();
        context.setVariable("recipeName", recipe.getName());
        context.setVariable("ingredients", recipe.getBody().getIngredients()
                .stream().map(e -> e.getData()).collect(Collectors.toList()));
        context.setVariable("steps", recipe.getBody().getSteps().stream()
                .map(e -> e.getData()).collect(Collectors.toList()));

        String process = templateEngine.process("templates/recipe", context);
        final String path = "Exports" + File.separator;
        OutputStream outputStream = new FileOutputStream(path + recipe.getId() + ".pdf");

        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(process);
        renderer.getFontResolver()
                .addFont("fonts/ARIALUNI.TTF", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        renderer.layout();
        renderer.setScaleToFit(true);
        renderer.createPDF(outputStream);

        outputStream.close();
    }

}
