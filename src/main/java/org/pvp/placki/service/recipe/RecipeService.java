package org.pvp.placki.service.recipe;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.pvp.placki.dto.recipe.DataToSaveDto;
import org.pvp.placki.dto.recipe.RecipeDto;
import org.pvp.placki.dto.recipe.RecipePageDto;
import org.pvp.placki.entity.appuser.AppUser;
import org.pvp.placki.entity.recipe.*;
import org.pvp.placki.repository.appuser.AppUserRepository;
import org.pvp.placki.repository.recipe.RecipeRepository;
import org.pvp.placki.repository.recipe.TagRepository;
import org.pvp.placki.service.export.PdfService;
import org.pvp.placki.util.SaveFileException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RecipeService {
    private final ModelMapper modelMapper;
    private static final int PAGE_SIZE = 8;
    private final RecipeRepository recipeRepository;
    private final TagRepository tagRepository;
    private final AppUserRepository appUserRepository;
    private final String pathToImages = "Photos" + File.separator;
    private final String defaultImage = pathToImages + "Default" + File.separator + "default.png";

    private Optional<AppUser> getAppUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<AppUser> appUser = appUserRepository.findByEmail(authentication.getPrincipal().toString());

        return appUser;
    }

    public RecipePageDto getSinglePage(int pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, PAGE_SIZE);
        return modelMapper.map(recipeRepository.findAll(pageRequest), RecipePageDto.class);
    }

    public RecipePageDto getSinglePageWithFiltersAndSorting(
            int pageNumber, Boolean descending, String containing,
            String fieldFilter, Boolean favorites) {
        PageRequest pageRequest;
        if (Objects.nonNull(fieldFilter) && !fieldFilter.isEmpty()) {
            if (descending) {
                pageRequest = PageRequest.of(pageNumber, PAGE_SIZE, Sort.by(fieldFilter).descending());
            } else {
                pageRequest = PageRequest.of(pageNumber, PAGE_SIZE, Sort.by(fieldFilter));
            }
        } else {
            pageRequest = PageRequest.of(pageNumber, PAGE_SIZE);
        }

        RecipePageDto recipePage;
        Optional<AppUser> appUser = getAppUser();
        if (Objects.nonNull(containing) && !containing.isEmpty()) {
            recipePage = modelMapper.map(favorites ?
                    recipeRepository.findAllByNameContainingIgnoreCaseAndFavorite(containing, appUser.get(), pageRequest)
                    : recipeRepository.findAllByNameContainingIgnoreCase(containing, pageRequest), RecipePageDto.class);
        } else {
            recipePage = modelMapper.map(
                    favorites ? recipeRepository.findAllByFavorite(appUser.get(), pageRequest)
                            : recipeRepository.findAll(pageRequest), RecipePageDto.class);

        }

        if (!appUser.isPresent()) {
            return recipePage;
        }

        List<Long> favoriteRecipes = appUser.get().getFavoriteRecipes().stream().map(Recipe::getId).collect(Collectors.toList());
        recipePage.getContent().stream().forEach(basicRecipeInfoDto -> {
            if (favoriteRecipes.contains(basicRecipeInfoDto.getRecipeId())) {
                basicRecipeInfoDto.setIsFavorite(true);
            } else {
                basicRecipeInfoDto.setIsFavorite(false);
            }
        });

        return recipePage;
    }

    public RecipeDto getRecipe(Long id) {
        RecipeDto recipe = modelMapper.map(recipeRepository.findById(id).get(), RecipeDto.class);

        Optional<AppUser> appUser = getAppUser();
        if (!appUser.isPresent()) {
            return recipe;
        }
        List<Long> favoriteRecipes = appUser.get().getFavoriteRecipes().stream().map(Recipe::getId).collect(Collectors.toList());

        if (favoriteRecipes.contains(recipe.getRecipeId())) {
            recipe.setIsFavorite(true);
        } else {
            recipe.setIsFavorite(false);
        }

        return recipe;
    }

    public RecipePageDto getUserSinglePage(int pageNumber) {
        PageRequest pageRequest = PageRequest.of(pageNumber, PAGE_SIZE - 1);

        AppUser appUser = getAppUser().get();

        RecipePageDto recipePage = modelMapper.map(recipeRepository.findAllByCreatorId(appUser.getId(), pageRequest), RecipePageDto.class);

        List<Long> favoriteRecipes = appUser.getFavoriteRecipes().stream().map(Recipe::getId).collect(Collectors.toList());
        recipePage.getContent().stream().forEach(basicRecipeInfoDto -> {
            if (favoriteRecipes.contains(basicRecipeInfoDto.getRecipeId())) {
                basicRecipeInfoDto.setIsFavorite(true);
            } else {
                basicRecipeInfoDto.setIsFavorite(false);
            }
        });

        return recipePage;
    }

    public Recipe saveRecipe(DataToSaveDto data, MultipartFile photo) {
        RecipeBody recipeBody = new RecipeBody();
        List<Ingredients> ingredients = data.toIngredients(recipeBody);
        List<Steps> steps = data.toSteps(recipeBody);

        recipeBody.setIngredients(ingredients);
        recipeBody.setSteps(steps);
        recipeBody.setServingQuantity(data.getServingQuantity());
        recipeBody.setTimeDescription(data.getTimeDescription());

        AppUser user = appUserRepository.findByEmail(data.getCreatorEmail()).get();

        String filename = "";
        if (Objects.nonNull(photo)) {
            filename = StringUtils.cleanPath(photo.getOriginalFilename());
            recipeBody.setPhoto(filename);
        }

        Recipe recipe = new Recipe(data.getRecipeName(), user, LocalDateTime.now(), null, recipeBody);

        List<Tag> tags = new ArrayList<>();
        data.getTags().stream().forEach(e -> {
            for (Tags t : Tags.values()) {
                if (t.name().equals(e.toUpperCase())) {
                    Tag tag = tagRepository.findTagByTagName(e);
                    tag.getRecipeList().add(recipe);
                    tags.add(tag);
                }
            }
        });
        recipe.setTags(tags);
        Recipe save = recipeRepository.save(recipe);

        if (Objects.nonNull(photo)) {
            String uploadDir = pathToImages + save.getId();

            try {
                saveFile(uploadDir, filename, photo);
            } catch (IOException ioe) {
                throw new SaveFileException(filename, ioe);
            }
        }

        PdfService.constructPdf(save);
        return recipe;
    }


    public void updateRecipe(Long id, DataToSaveDto data, MultipartFile photo) {
        RecipeBody recipeBody = new RecipeBody();
        List<Ingredients> ingredients = data.toIngredients(recipeBody);
        List<Steps> steps = data.toSteps(recipeBody);

        recipeBody.setIngredients(ingredients);
        recipeBody.setSteps(steps);
        recipeBody.setServingQuantity(data.getServingQuantity());
        recipeBody.setTimeDescription(data.getTimeDescription());

        Recipe recipe = recipeRepository.findById(id).get();
        recipeBody.setPhoto(recipe.getBody().getPhoto());

        if (Objects.nonNull(photo)) {

            String filename = StringUtils.cleanPath(photo.getOriginalFilename());
            String uploadDir = pathToImages + id;

            deletePhoto(uploadDir, recipe.getBody().getPhoto());

            try {
                saveFile(uploadDir, filename, photo);
            } catch (IOException ioe) {
                throw new SaveFileException(filename, ioe);
            }
            recipeBody.setPhoto(filename);
        }

        recipe.setName(data.getRecipeName());
        recipe.setLastModified(LocalDateTime.now());
        recipe.setBody(recipeBody);
        List<Tag> tags = new ArrayList<>();
        data.getTags().stream().forEach(e -> {
            for (Tags t : Tags.values()) {
                if (t.name().equals(e.toUpperCase())) {
                    Tag tag = tagRepository.findTagByTagName(e);
                    tag.getRecipeList().add(recipe);
                    tags.add(tag);
                }
            }
        });
        recipe.setTags(tags);
        Recipe save = recipeRepository.save(recipe);

        PdfService.constructPdf(save);
    }

    public static void saveFile(String uploadDir, String fileName,
                                MultipartFile multipartFile) throws IOException {
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        InputStream inputStream = multipartFile.getInputStream();
        Path filePath = uploadPath.resolve(fileName);
        Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);

    }

    public File getPhoto(Long recipeId) {
        Recipe recipe = recipeRepository.findById((recipeId)).get();
        String pathName = pathToImages + recipeId + File.separator + recipe.getBody().getPhoto();
        File photo = new File(pathName);
        if (!photo.exists()) {
            return new File(defaultImage);
        }
        return photo;
    }

    public boolean deletePhoto(String dir, String photo) {
        String pathName = dir + File.separator + photo;
        File photoToDelete = new File(pathName);
        return photoToDelete.delete();
    }

    public void addMark(Long id, Float value) {
        AppUser appUser = getAppUser().get();
        Recipe recipe = recipeRepository.getOne(id);
        List<Mark> marks = recipe.getMarks();
        MarkId markId = new MarkId(id, appUser.getId());
        Mark mark = new Mark(markId, recipe, appUser, value);
        marks.add(mark);
        recipe.setMarks(marks);
        recipe = recipeRepository.save(recipe);
        Float newMarkAvg = BigDecimal.valueOf(recipeRepository.getMarkAvg(id).orElse(value.doubleValue())).setScale(2, RoundingMode.HALF_UP).floatValue();
        recipe.setMarksAvg(newMarkAvg);
        recipeRepository.save(recipe);
    }

    public void addComment(Long id, String content) {
        AppUser appUser = getAppUser().get();
        Recipe recipe = recipeRepository.getOne(id);
        RecipeBody recipeBody = recipe.getBody();
        List<Comment> comments = recipeBody.getComments();
        Comment newComment = new Comment(
                content, appUser, recipeBody, LocalDateTime.now()
        );
        comments.add(newComment);
        recipeBody.setComments(comments);

        recipeRepository.save(recipe);
    }


    public void addFavorite(Long id) {
        AppUser appUser = getAppUser().get();
        Recipe recipe = recipeRepository.getOne(id);
        recipe.getFavorite().add(appUser);
        appUser.getFavoriteRecipes().add(recipe);
        appUserRepository.save(appUser);
    }

    public void deleteFavorite(Long id) {
        AppUser appUser = getAppUser().get();
        Recipe recipe = recipeRepository.getOne(id);
        recipe.getFavorite().remove(appUser);
        appUser.getFavoriteRecipes().remove(recipe);
        appUserRepository.save(appUser);
    }
}
