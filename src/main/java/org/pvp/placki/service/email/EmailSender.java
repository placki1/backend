package org.pvp.placki.service.email;

public interface EmailSender {
    void send(String to, String email);
}