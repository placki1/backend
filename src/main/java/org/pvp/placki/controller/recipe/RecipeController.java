package org.pvp.placki.controller.recipe;

import lombok.AllArgsConstructor;
import org.pvp.placki.dto.recipe.DataToSaveDto;
import org.pvp.placki.dto.recipe.RecipeDto;
import org.pvp.placki.dto.recipe.RecipePageDto;
import org.pvp.placki.entity.recipe.Recipe;
import org.pvp.placki.service.recipe.RecipeService;
import org.pvp.placki.util.InvalidPhotoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1/recipe/")
public class RecipeController {
    private final RecipeService recipeService;

    @GetMapping("pages/{pageNumber}")
    public RecipePageDto getRecipesPage(@PathVariable Integer pageNumber) {
        return recipeService.getSinglePage(pageNumber);
    }

    @GetMapping("user/page/{pageNumber}")
    public RecipePageDto getUserRecipesPage(@PathVariable Integer pageNumber) {
        return recipeService.getUserSinglePage(pageNumber);
    }

    @GetMapping("page/{pageNumber}")
    public RecipePageDto getRecipePageWithFiltersAndSorting(
            @PathVariable Integer pageNumber,
            @RequestParam(required=false) String containing,
            @RequestParam(value="descending", defaultValue="false", required=false) Boolean descending,
            @RequestParam(required=false) String fieldFilter,
            @RequestParam(value="favorites", defaultValue="false", required=false) Boolean favorites) {
        return recipeService.getSinglePageWithFiltersAndSorting(pageNumber, descending, containing, fieldFilter, favorites);
    }

    @GetMapping("{id}")
    public RecipeDto getRecipe(@PathVariable Long id) {
        return recipeService.getRecipe(id);
    }


    @PostMapping("{id}/mark")
    public String addMark(@PathVariable Long id, @RequestBody Float value) {
        recipeService.addMark(id, value);
        return "add";
    }

    @PostMapping("{id}/comment")
    public ResponseEntity addComment(@PathVariable Long id, @RequestBody String content) {
        recipeService.addComment(id, content);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("photo/{id}")
    public ResponseEntity<byte[]> getPhoto(@PathVariable Long id) {

        File photo = recipeService.getPhoto(id);
        String fileType = "Undetermined";
        byte[] content;
        try {
            fileType = Files.probeContentType(photo.toPath());
            content = Files.readAllBytes(photo.toPath());
        } catch (IOException e) {
            throw new InvalidPhotoException(photo.getName(), e);
        }

        return ResponseEntity
                .ok()
                .contentType(MediaType.valueOf(fileType))
                .body(content);
    }

    @PostMapping("/addRecipe")
    public ResponseEntity addRecipe(@RequestPart("data") DataToSaveDto data,
                                    @RequestPart(name="photo", required=false) MultipartFile photo) {
        Recipe recipe = recipeService.saveRecipe(data, photo);
        return new ResponseEntity(recipe.getId(), HttpStatus.CREATED);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity updateRecipe(@PathVariable Long id,
                                       @RequestPart("data") DataToSaveDto data,
                                       @RequestPart(name="photo", required=false) MultipartFile photo) {
        recipeService.updateRecipe(id, data, photo);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/{id}/favorites/add")
    public ResponseEntity addFavorite(@PathVariable Long id){
        recipeService.addFavorite(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/{id}/favorites/remove")
    public ResponseEntity deleteFavorite(@PathVariable Long id){
        recipeService.deleteFavorite(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
