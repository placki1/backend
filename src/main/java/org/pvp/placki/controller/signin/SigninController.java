package org.pvp.placki.controller.signin;


import org.pvp.placki.configuration.security.config.login.LoginCredentials;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/signin")
public class SigninController {

    @PostMapping
    public void login(@RequestBody LoginCredentials credentials) {
    }

    @GetMapping("/h")
    @ResponseBody
    public String test(){
        return "hello log in";
    }

}
