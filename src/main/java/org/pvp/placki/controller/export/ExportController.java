package org.pvp.placki.controller.export;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.pvp.placki.dto.recipe.RecipePageDto;
import org.pvp.placki.entity.recipe.Recipe;
import org.pvp.placki.repository.recipe.RecipeRepository;
import org.pvp.placki.service.export.PdfService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@AllArgsConstructor
@RequestMapping("/export/")
public class ExportController {

    private final RecipeRepository recipeRepository;

    @SneakyThrows
    @GetMapping("/{id}")
    public ResponseEntity<byte[]> getExport(@PathVariable Long id) {
        final String path = "Exports" + File.separator;
        Path file = Paths.get(path, id + ".pdf");

        if (!Files.exists(file)){
            Recipe recipe = recipeRepository.findById(id).get();
            PdfService.constructPdf(recipe);
        }

        var pdf = Files.readAllBytes(file);
        return ResponseEntity
                .ok()
                .contentType(MediaType.valueOf("application/pdf"))
                .body(pdf);
    }
}
