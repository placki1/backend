package org.pvp.placki.util;

public class InvalidPhotoException extends RuntimeException{
    public InvalidPhotoException (String photoName, Throwable exception) {
        super("Invalid photo:" + photoName, exception);
    }
}
