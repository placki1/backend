package org.pvp.placki.util;

public class SaveFileException extends RuntimeException{
    public SaveFileException (String filename, Throwable exception) {
        super("Could not save image file: " + filename, exception);
    }
}