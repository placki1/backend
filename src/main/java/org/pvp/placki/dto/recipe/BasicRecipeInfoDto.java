package org.pvp.placki.dto.recipe;

import lombok.Data;
import org.pvp.placki.entity.recipe.Tag;
import java.time.LocalDateTime;
import java.util.List;


@Data
public class BasicRecipeInfoDto {
    private long recipeId;
    private String recipeName;
    private long creatorId;
    private LocalDateTime createdOn;
    private String creatorUsername;
    private List<Tag> tags;
    private boolean isFavorite;
    private Float marksAvg;

    public void setIsFavorite(boolean isFavorite){
        this.isFavorite = isFavorite;
    }
}
