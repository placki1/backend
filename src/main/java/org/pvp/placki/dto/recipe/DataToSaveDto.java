package org.pvp.placki.dto.recipe;

import lombok.Data;
import org.pvp.placki.entity.recipe.Ingredients;
import org.pvp.placki.entity.recipe.RecipeBody;
import org.pvp.placki.entity.recipe.Steps;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class DataToSaveDto {
    private String recipeName;
    private String createdOn;
    private String creatorUsername;
    private String creatorEmail;
    private List<String> tags;
    private Float servingQuantity;
    private String timeDescription;
    private List<StepsDto> steps;
    private List<StepsDto> ingredients;

    public List<Ingredients> toIngredients(final RecipeBody recipeBody) {
        return getIngredients().stream().map(e -> new Ingredients(e.getData(), e.getOrderNumber(), recipeBody)).collect(Collectors.toList());
    }

    public List<Steps> toSteps(final RecipeBody recipeBody) {
        return getSteps().stream().map(e -> new Steps(e.getData(), e.getOrderNumber(), recipeBody)).collect(Collectors.toList());
    }
}
