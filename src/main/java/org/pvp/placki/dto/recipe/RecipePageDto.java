package org.pvp.placki.dto.recipe;

import lombok.Data;
import java.util.List;

@Data
public class RecipePageDto {
    private int totalPages;
    private int pageNumber;
    private List<BasicRecipeInfoDto> content;
}
