package org.pvp.placki.dto.recipe;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class CommentDto {
    private String content;
    private String creatorUsername;
    private LocalDateTime createdOn;
}
