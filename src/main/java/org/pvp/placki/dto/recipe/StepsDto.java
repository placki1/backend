package org.pvp.placki.dto.recipe;

import lombok.Data;

@Data
public class StepsDto {
    private String data;
    private Integer orderNumber;
}
