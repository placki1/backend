package org.pvp.placki.dto.recipe;

import lombok.Data;
import org.pvp.placki.entity.recipe.Ingredients;
import org.pvp.placki.entity.recipe.Steps;

import java.time.ZoneOffset;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Data
public class RecipeBodyDto {
    private Long id;
    private Float servingQuantity;
    private String timeDescription;
    private String photo;
    private List<Steps> steps;
    private List<Ingredients> ingredients;
    private List<CommentDto> comments;

    public List<CommentDto> getComments() {
        Comparator<CommentDto> comparator = Comparator.comparingLong(comment -> comment.getCreatedOn().toEpochSecond(ZoneOffset.UTC));
        Collections.sort(comments, comparator.reversed());
        return comments;
    }
}
