package org.pvp.placki;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
@Slf4j
public class PlackiApplication {

    public static void main(String[] args) {
        log.info("Testing logger config");
        SpringApplication.run(PlackiApplication.class, args);
    }
}