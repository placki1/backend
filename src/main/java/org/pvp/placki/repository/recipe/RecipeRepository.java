package org.pvp.placki.repository.recipe;

import org.pvp.placki.entity.appuser.AppUser;
import org.pvp.placki.entity.recipe.Recipe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    Page<Recipe> findAllByFavorite(AppUser appUser, Pageable pageable);

    Page<Recipe> findAllByCreatorId(Long id, Pageable pageable);

    Page<Recipe> findAllByNameContainingIgnoreCase(String name, Pageable pageable);

    Page<Recipe> findAllByNameContainingIgnoreCaseAndFavorite(String name, AppUser appUser, Pageable pageable);

    @Query("select avg(m.value) FROM Mark m WHERE m.id.recipeId = ?1")
    Optional<Double> getMarkAvg(Long id);
}
