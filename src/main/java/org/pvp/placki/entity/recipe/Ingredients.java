package org.pvp.placki.entity.recipe;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
public class Ingredients {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String data;
    private long orderNumber;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="bodyRecipe_id")
    private RecipeBody recipeBody;

    public Ingredients(String data, long orderNumber, RecipeBody recipeBody) {
        this.data = data;
        this.orderNumber = orderNumber;
        this.recipeBody = recipeBody;
    }
}
