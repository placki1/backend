package org.pvp.placki.entity.recipe;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String tagName;

    @JsonIgnore
    @ManyToMany(mappedBy = "tags")
    private List<Recipe> recipeList = new ArrayList<>();

    public Tag(String tagName, Recipe recipe) {
        this.tagName = tagName;
        this.recipeList.add(recipe);
    }

}