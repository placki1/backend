package org.pvp.placki.entity.recipe;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Entity
@Data
@NoArgsConstructor
public class RecipeBody {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    @OneToOne(mappedBy = "body")
    private Recipe recipe;

    private String photo;
    private Float servingQuantity;
    private String timeDescription;

    @OneToMany(mappedBy = "recipeBody", cascade = CascadeType.ALL)
    private List<Steps> steps;
    @OneToMany(mappedBy = "recipeBody", cascade = CascadeType.ALL)
    private List<Ingredients> ingredients;
    @OneToMany(mappedBy = "recipeBody", cascade = CascadeType.ALL)
    private List<Comment> comments;
}
