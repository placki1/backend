package org.pvp.placki.entity.recipe;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pvp.placki.entity.recipe.RecipeBody;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
public class Steps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String data;
    private long orderNumber;
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="bodyRecipe_id")
    private RecipeBody recipeBody;

    public Steps(String data, long orderNumber, RecipeBody recipeBody) {
        this.data = data;
        this.orderNumber = orderNumber;
        this.recipeBody = recipeBody;
    }
}
