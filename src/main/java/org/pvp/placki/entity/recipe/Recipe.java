package org.pvp.placki.entity.recipe;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.pvp.placki.entity.appuser.AppUser;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    private AppUser creator;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "recipies_tags",
            joinColumns = {@JoinColumn(name = "recipe_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id", nullable = false, updatable = false)})
    private List<Tag> tags;

    @OneToMany(mappedBy = "recipe", cascade = CascadeType.ALL)
    private List<Mark> marks;
    private Float marksAvg = 0.0f;

    private LocalDateTime createdOn;
    private LocalDateTime lastModified;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "body_id")
    private RecipeBody body;

    @ManyToMany
    @JoinTable(name = "favorites",
            joinColumns = @JoinColumn(name = "recipe_id"),
            inverseJoinColumns = @JoinColumn(name = "app_user_id"))
    private List<AppUser> favorite;

    public Recipe(String name, AppUser creator, LocalDateTime createdOn, LocalDateTime lastModified, RecipeBody body) {
        this.name = name;
        this.creator = creator;
        this.createdOn = createdOn;
        this.lastModified = lastModified;
        this.body = body;
    }
}
