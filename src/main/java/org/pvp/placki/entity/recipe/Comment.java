package org.pvp.placki.entity.recipe;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pvp.placki.entity.appuser.AppUser;
import org.pvp.placki.entity.recipe.RecipeBody;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String content;

    private LocalDateTime createdOn;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    private AppUser creator;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="body_recipe_id")
    private RecipeBody recipeBody;

    public Comment(String content, AppUser creator, RecipeBody recipeBody, LocalDateTime createdOn){
        this.content = content;
        this.creator = creator;
        this.recipeBody = recipeBody;
        this.createdOn = createdOn;
    }
}
