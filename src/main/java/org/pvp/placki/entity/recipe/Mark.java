package org.pvp.placki.entity.recipe;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.pvp.placki.entity.appuser.AppUser;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Mark {
    @EmbeddedId
    private MarkId id;
    @JoinColumn(name = "recipe_id", insertable = false, updatable = false)
    @JsonIgnore
    @ManyToOne
    private Recipe recipe;
    @JoinColumn(name = "app_user_id", insertable = false, updatable = false)
    @JsonIgnore
    @ManyToOne
    private AppUser appUser;
    private Float value;
}

