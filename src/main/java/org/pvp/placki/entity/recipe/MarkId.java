package org.pvp.placki.entity.recipe;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MarkId implements Serializable {
    @Column(name = "recipe_id")
    private Long recipeId;
    @Column(name = "app_user_id")
    private Long appUserId;
}
