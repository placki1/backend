package org.pvp.placki.configuration.security.config.login;

import lombok.Getter;

@Getter
public class LoginCredentials {
    private String email;
    private String password;
}
