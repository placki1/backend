drop table if exists tag CASCADE;
drop table if exists recipies_tags CASCADE;

create table tag (
    id bigint NOT NULL IDENTITY PRIMARY KEY,
    tag_name varchar(255) not null unique
);

create table recipies_tags (
    recipe_id bigint not null,
    tag_id bigint not null,
    FOREIGN KEY (recipe_id) REFERENCES recipe (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY (tag_id) REFERENCES tag (id) ON DELETE RESTRICT ON UPDATE CASCADE,
    PRIMARY KEY (recipe_id, tag_id)
);