drop table if exists steps CASCADE;
create table steps (
    id bigint NOT NULL IDENTITY PRIMARY KEY,
    data varchar(2500),
    order_number bigint not null,
    body_recipe_id bigint
);

alter table steps add constraint FK_RECIPE_BODY_STEPS foreign key (body_recipe_id) references recipe_body;