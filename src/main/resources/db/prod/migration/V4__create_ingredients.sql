drop table if exists ingredients CASCADE;
create table ingredients (
    id bigint NOT NULL IDENTITY PRIMARY KEY,
    data varchar(255),
    order_number bigint not null,
    body_recipe_id bigint
);

alter table ingredients add constraint FK_RECIPE_BODY_INGREDIENTS foreign key (body_recipe_id) references recipe_body;