CREATE TABLE mark (
    recipe_id bigint not null,
    app_user_id bigint not null,
    value float not null,
    FOREIGN KEY (recipe_id) REFERENCES recipe (id),
    FOREIGN KEY (app_user_id) REFERENCES app_user (id),
    PRIMARY KEY (recipe_id, app_user_id)
);