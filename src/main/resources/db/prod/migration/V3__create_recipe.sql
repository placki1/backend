drop table if exists recipe CASCADE;
drop table if exists recipe_body CASCADE;

create table recipe (
    id bigint NOT NULL IDENTITY PRIMARY KEY,
    created_on timestamp,
    last_modified timestamp,
    name varchar(255),
    body_id bigint,
    creator_id bigint,
    marks_avg FLOAT DEFAULT 0.0
);

create table recipe_body (
    id bigint not null identity primary key,
    serving_quantity float,
    photo varchar(255),
    time_description varchar(255)
);

alter table recipe add constraint FK_RECIPE_BODY_RECIPE foreign key (body_id) references recipe_body;
alter table recipe add constraint FK_RECIPE_APP_USER foreign key (creator_id) references app_user;