
INSERT INTO app_user (app_user_role, email, enabled, username, locked, password) VALUES ('ADMIN', 'admin', true, 'admin', false, '$2b$10$jAaUfdvfXgDJlV5DxD.i4OZ9cFNX7kNwMKTY.aZScer0Ghjmj.NXC');
INSERT INTO app_user (app_user_role, email, enabled, username, locked, password) VALUES ('ADMIN', 'admin@admin.com', true, 'admin', false, '$2b$10$jAaUfdvfXgDJlV5DxD.i4OZ9cFNX7kNwMKTY.aZScer0Ghjmj.NXC');