drop table if exists comment CASCADE;
create table comment (
                       id bigint NOT NULL IDENTITY PRIMARY KEY,
                       created_on timestamp,
                       content text,
                       creator_id bigint,
                       body_recipe_id bigint
);

alter table comment add constraint FK_COMMENT_APP_USER foreign key (creator_id) references app_user;
alter table comment add constraint FK_COMMENT_BODY_RECIPE foreign key (body_recipe_id) references recipe_body;