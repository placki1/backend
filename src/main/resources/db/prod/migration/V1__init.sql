drop table if exists app_user CASCADE;
drop table if exists confirmation_token CASCADE;

create table app_user (
    id bigint NOT NULL IDENTITY PRIMARY KEY,
    app_user_role varchar(255),
    email varchar(255) NOT NULL UNIQUE,
    enabled boolean,
    username varchar(255),
    locked boolean,
    password varchar(255)
);

create table confirmation_token (
    id bigint NOT NULL IDENTITY PRIMARY KEY,
    confirmed_at timestamp,
    created_at timestamp not null,
    expired_at timestamp not null,
    token varchar(255) not null,
    app_user_id bigint not null
);

alter table confirmation_token add constraint FKo9fl25wqyh7w7mnfkdqen1rcm foreign key (app_user_id) references app_user;