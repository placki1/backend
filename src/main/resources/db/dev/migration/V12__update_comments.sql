
-- password = koparka1
INSERT INTO app_user (app_user_role, email, enabled, username, locked, password) VALUES ('USER', 'bob@gmail.com', true, 'Bob Kowalski', false, '$2y$10$GHDwJ5NttZG3GQzrC7iFO.VGT6rth2p88jVLmgK0BWKR9AFy1F2Ka');

-- password = goldenWind
INSERT INTO app_user (app_user_role, email, enabled, username, locked, password) VALUES ('USER', 'giorgio@gmail.com', true, 'Giovanni Giorgio Moroder', false, '$2y$10$4dd5iAeqGsWF5OnxOq.v4OBArbTKOZ6V4PE2uh3oq7Nt4f2v7KPSu');

SET @user1ID = SELECT ID FROM app_user WHERE email = 'adam@gmail.com';
SET @user2ID = SELECT ID FROM app_user WHERE email = 'malykotek@interia.pl';
SET @user3ID = SELECT ID FROM app_user WHERE email = 'bob@gmail.com';
SET @user4ID = SELECT ID FROM app_user WHERE email = 'giorgio@gmail.com';
SET @creatorID = SELECT ID FROM app_user WHERE email = 'admin@admin.com';

INSERT INTO comment(content, created_on, creator_id, body_recipe_id)
VALUES ('10/10 boskie!',  '2020-12-02 12:00:01', @user1ID, 1),
       ('PRZEPYSZNE, MÓJ SYNEK BARDZO LUBI', '2020-12-21 9:00:01', @user2ID, 1),
       ('Za drugim razem jeszcze lepsze !', '2021-01-05 21:37:00', @user3ID, 1),
       ('Delizioso', '2021-02-15 13:30:01', @user4ID, 1),

       ('BABCIU ALE TEN KOTLET JEST PYSZNY', '2020-12-02 12:30:01' , @user1ID, 2),
       ('Ależ on żre ten koperek, to będzie bardzo zdrowy piesek', '2020-12-21 21:37:00', @user2ID,2),
       ('Smaczne i proste!',  '2021-01-08 04:20:01', @user3ID, 2),
       ('Dios mio, delizioso', '2021-02-15 15:30:01', @user4ID, 2),

       ('potężnie pyszne kulki ;)', '2019-01-03 00:51:32', @user1ID, 3),
       ('Lepsze niż te z kryszny !', '2019-02-15 00:51:32', @user2ID, 3),
       ('kamehameha',  '2019-10-25 04:20:01', @user3ID, 3),
       ('Potente', '2019-12-30 23:59:00', @user4ID, 3);

