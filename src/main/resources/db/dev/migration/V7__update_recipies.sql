-- -- new users
-- password = Qwerty12345!
INSERT INTO app_user (app_user_role, email, enabled, username, locked, password) VALUES ('USER', 'adam@gmail.com', true, 'Adam Malinowski', false, '$2a$12$EyrZqJ12yihLSMh3x0I2M.lAzJTWh5jfZjJhsXRq4I68JhmChALfO');
-- password = Kociak12345678!
INSERT INTO app_user (app_user_role, email, enabled, username, locked, password) VALUES ('USER', 'malykotek@interia.pl', true, 'Alina Kotkowska', false, '$2a$12$IrIRgM2nnzbNnEWSO/iQHe24i6kQ8ibB2/4/OvRifGof8nguFxLiu');
SET @user1ID = SELECT ID FROM app_user WHERE email = 'adam@gmail.com';
SET @user2ID = SELECT ID FROM app_user WHERE email = 'malykotek@interia.pl';

SET @creatorID = SELECT ID FROM app_user WHERE email = 'admin@admin.com';

-- tagi
INSERT INTO tag(tag_name) VALUES ('Śniadanie');
INSERT INTO tag(tag_name) VALUES ('Obiad');
INSERT INTO tag(tag_name) VALUES ('Kolacja');
INSERT INTO tag(tag_name) VALUES ('Lunch');
INSERT INTO tag(tag_name) VALUES ('Deser');

-- przepis 1
-- -- recipe body
INSERT INTO recipe_body(serving_quantity,photo, time_description) VALUES (3.0,'wolowina_pieczary.jpg' ,'2h');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2020-12-01', NULL, 'Wołowina w pieczarkach', @sc_id, @creatorID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 Pieczarki', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2kg Wołowiny', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('500ml wody', 2, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Umyj pieczarki i zalej wodą', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Podsmaż wołowinę', 1, @sc_id);

-- -- tagi;
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 1);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 3);

-- przepis 2
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (1.0, 'kotlet-schabowy-w-smietanie0-4.jpg', '30min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2020-12-01', NULL, 'Schabowy', @sc_id, @creatorID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('600g schabu', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('sól', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('pieprz', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('mleko', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 jajka', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('bułka tarta', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('masło klarowane', 6, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Ostrym nożem odciąć białą otoczkę z żyłki po zewnętrznej części mięsa. Pokroić na 4 plastry. Położyć na desce i dokładnie roztłuc na cieniutkie filety (mogą wyjść duże, wielkości pół talerza). Do rozbicia mięsa najlepiej użyć specjalnego tłuczka z metalowym obiciem z wytłoczoną krateczką.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Filety namoczyć w mleku z dodatkiem soli i pieprzu (ewentualnie z dodatkiem kilku plastrów cebuli) przez ok. 2 godziny lub dłużej jeśli mamy czas (można też zostawić do namoczenia na noc).', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wyjąć filety z mleka i osuszyć je papierowymi ręcznikami. Doprawić solą (niezbyt dużo, bo zalewa z mleka była już solona) i pieprzem, obtoczyć w mące, następnie w roztrzepanym jajku, a na koniec w bułce tartej.', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Na patelni rozgrzać klarowane masło (ok. 1/2 cm warstwa) lub smalec. Smażyć partiami po 2 kotlety, na większym ogniu, po 2 minuty z każdej strony. Następnie zmniejszyć ogień i smażyć jeszcze po ok. 3 minuty z każdej strony. Przetrzeć patelnię papierowym ręcznikiem i powtórzyć z kolejną partią, na świeżym tłuszczu.', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Usmażone schabowe odsączyć z tłuszczu na papierowym ręczniku i podawać z ziemniakami i kapustą lub mizerią.', 4, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 2);


-- przepis 3
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (1.0, 'df3330eaa5a434d5d95b5e7f25aff928_369065_5b4f70891568b_wm.jpeg', '1.30min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2019-01-01', NULL, 'Kulki mocy', @sc_id, @creatorID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 szklanka daktylki bez pestek (miękkich)', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('3/4 szklanki zmielonych migdałów', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/2 szklanki wiórków kokosowych', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 łyżki nasion chia (opcjonalnie)', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka oleju kokosowego', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 czubata łyżka masła orzechowego (np. z kawałkami orzechów)', 5, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wszystkie składniki umieścić w melakserze/rozdrabniaczu/thermomixie i rozdrobnić na pastę z małymi cząstkami składników.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Uformować kulki wielkości orzecha włoskiego dociskając masę i wstawić do lodówki na kilka godzin, aby nabrały bardziej stałej konsystencji i się nie kruszyły.', 1, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 5);

-- przepis 4
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (10.0, 'deser_tiramisu_0.jpg', '1.30min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-12-01', NULL, 'Tiramisu', @sc_id, @creatorID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('350 ml zaparzonej kawy espresso', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('25 ml likieru amaretto lub Maraschino', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 jajka + 2 żółtka', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('70 g cukru pudru', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('500 g serka mascarpone', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('300 g podłużnych biszkoptów', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('ok. 3 łyżek gorzkiego kakao', 6, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Jajka włożyć do zlewu i przelać gorącą wodą. Zaparzyć kawę, dodać likier i całość ostudzić. Oddzielić żółtka od białek.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wszystkie 4 żółtka ubić z cukrem pudrem na puszysty i jasny krem (ok. 7 minut) - najlepiej początkowo ubijać na parze (w metalowej misce zawieszonej na garnku z parującą wodą), a gdy żółtka będą już ciepłe, odstawić z pary i dalej ubijać.', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Następnie dodawać porcjami (po 3 łyżki) mascarpone, ale już w krótszych odstępach czasu, cały czas ubijając, aż krem będzie gęsty i jednolity.', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('W oddzielnej misce ubić białka na idealnie sztywną pianę z dodatkiem małej szczypty soli. Połączyć je z kremem z żółtek i mascarpone delikatnie mieszając łyżką.', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Połowę biszkoptów na moment zanurzać w kawie i układać w prostokątnym naczyniu np. żaroodpornym lub szklanym (ok. 20 x 22 cm) lub w pojedynczych naczynkach. Posypać cienką warstwą kakao.', 4, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wyłożyć połowę kremu i przykryć kolejną warstwą nasączonych biszkoptów. Znów oprószyć kakao. Posmarować resztą kremu, posypać kakao i wstawić do lodówki na minimum 3 godziny lub na całą noc.', 5, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 5);

-- przepis 5
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (10.0, 'z24376255Q,Babeczki-czekoladowe--po-ostudzeniu--mozna-udekoro.jpg', '1.30min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-12-01', NULL, 'Muffinki czekoladowe bez mąki', @sc_id, @creatorID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('3 banany', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka soku z cytryny', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 jajka', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/4 szklanki miodu', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 niepełna szklanka (200 ml) masła orzechowego (bez soli i cukru)', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/4 szklanki gorzkiego kakao', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżeczka sody oczyszczonej', 6, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Piekarnik nagrzać do 180 stopni C..', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Do miski włożyć obrane i pokrojone na kawałki banany, skropić sokiem z cytryny.', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Dodać jajka, miód, masło orzechowe i wszystko rozgnieść widelcem..', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Dodać kakao oraz sodę oczyszczoną i wymieszać wszystko rózgą.', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Formę na muffiny wyłożyć papilotkami, napełnić je masą i posypać dropsami czekoladowymi. ', 4, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wstawić do piekarnika i piec przez ok. 18 - 20 minut.', 5, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 5);

-- przepis 6
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (1.0, 'makaron-z-krewetkami-cajun-000.jpg', '6.30min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-12-01', NULL, 'Makaron z krewetkami w sosie cajun', @sc_id, @creatorID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('ok. 400 g krewetek', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('250 g makaronu spaghetti', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka oleju roślinnego', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 ząbki czosnku', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('3 łyżki masła', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/2 szklanki bulionu', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka słodkiego sosu chili', 6, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka sosu worcestershire', 7, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 łyżki soku z cytryny', 8, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka posiekanej natki pietruszki', 9, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Krewetki rozmrozić jeśli były zamrożone, oczyścić i oderwać ogonki..', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Przygotować mieszankę przypraw Cajun (połączyć wszystkie przyprawy razem).', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Nastawić wodę na makaron w dużym garnku i posolić ją. Wrzucić makaron na wrzątek i gotować według instrukcji na opakowaniu (ok. 10 - 12 minut).', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Na dużą patelnię wlać olej, dodać przeciśnięty przez praskę czosnek i podsmażać przez ok. pół minuty.', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Dodać 1 łyżkę masła i po roztopieniu włożyć krewetki. Posypać połową mieszanki przypraw, przewrócić na drugą stronę po minucie smażenia, posypać resztą przypraw i smażyć przez kolejną minutę. ', 4, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wlać gorący bulion, następnie słodki sos chili, sos worcestershire i sok z cytryny. Gotować przez ok. 15 sekund. Na koniec dodać i rozpuścić resztę masła oraz wymieszać z natką pietruszki.', 5, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Makaron włożyć na patelnię z krewetkami, wymieszać i chwilę razem podgrzać. Podawać z cząstkami cytryny.', 6, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 1);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 2);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 4);

-- przepis 7
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (1.0, 'indyk-2-1300x740.jpg', '6.30min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-12-01', NULL, 'Indyk miodowo-musztardowy', @sc_id, @creatorID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżeczka soli morskiej', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/2 łyżeczki świeżo zmielonego pieprzu', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('3 ząbki czosnku (obrane)', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 gałązka rozmarynu (listki)', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka suszonego tymianku', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 łyżki musztardy', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1,5 łyżki miodu lub syropu klonowego', 6, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 łyżki oliwy lub oleju', 7, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 łyżki sosu worcestershire', 8, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 łyżki sosu sojowego', 9, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Filet włożyć do niedużego naczynia (tak aby mięso ciasno do niego wchodziło), dodać wszystkie składniki marynaty. Obtoczyć w nich mięso i odłożyć pod przykryciem do lodówki na całą noc.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Piekarnik nagrzać do 180 stopni C. Mięso wyjąć z naczynia i zawiązać sznurkiem nadając mu ładniejszego kształtu. Filet położyć w naczyniu żaroodpornym lub blaszce do pieczenia, polać marynatą z naczynia i wlać dodatkowo wrzącą wodę (razem z marynatą ok. 1 cm). Wstawić do piekarnika.', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Piec przez 80 minut. W trakcie pieczenia polewać mięso sokiem z naczynia, mniej więcej co 15 minut.', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Mięso wyjąć w piekarnika i odczekać ok. 10 minut przed pokrojeniem. Udekorować opcjonalnie granatem i rozmarynem i wykorzystać z sosem do obiadu.', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Można też po po ostudzeniu zawinąć w folię i odłożyć do lodówki i wykorzystać do kanapek. Sos z pieczeni przecedzić, zlać do garnuszka i wykorzystać jako sos (połączyć z majonezem). ', 4, @sc_id);
-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 1);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 2);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 4);

-- przepis 8
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (1.0, 'losos-ze-szparagami-01.jpg', '30min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-12-01', NULL, 'Łosoś se szparagami', @sc_id, @creatorID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('500 g filetu łososia', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 cytryna', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 pęczek szparagów', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('25 g masła', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 ząbki czosnku', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('koperek', 5, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Piekarnik nagrzać do 230 stopni C.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Łososia pokroić na porcje, ułożyć na desce skórką do dołu i przesuwając nóż wzdłuż filetu pomiędzy skórką a mięsem, odciąć skórę.', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Filety opłukać, osuszyć, doprawić solą, pieprzem oraz skropić połową cytryny (drugą połowę pokroić na plasterki).', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Szparagi umyć, odłamać twarde końce (same złamią się w odpowiednim miejscu). Ułożyć razem z łososiem w naczyniu żaroodpornym.', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Masło zmiękczyć (albo wcześniej wyjąć z lodówki lub delikatnie podgrzać). Dodać przeciśnięty przez praskę czosnek, łyżkę posiekanego koperku oraz sól i pieprz.', 4, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wymieszać i posmarować otrzymanym masłem wierzch łososia oraz szparagi. Na rybie położyć półplasterki pozostałej cytryny.', 5, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wstawić do piekarnika i piec przez 10 minut bez przykrycia, następnie ustawić grill w piekarniku i piec jeszcze przez 3 - 4 minuty (jeśli nie mamy grilla to pieczemy jak wyżej w 230 stopniach).', 6, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 1);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 2);

-- przepis 9
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (1.0, 'musli_dr_birchera_01.jpg', '30min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2018-09-12', NULL, 'Musli Dd Birchera', @sc_id, @creatorID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('80 g (3/4 szklanki) płatków owsianych górskich', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('300 ml soku jabłkowego', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 jabłko', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 banan', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('sok z 1/2 cytryny', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('100 g jogurtu naturalnego', 5, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wieczorem poprzedniego dnia wsypać płatki do miski i wymieszać z sokiem jabłkowym, odstawić w chłodniejsze miejsce (np. na parapet okna).', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Rano dodać do płatków starte na dużych oczkach jabłko (można obrać lub zetrzeć ze skórką) oraz banana. Na koniec wymieszać wszystko z sokiem z cytryny i jogurtem naturalnym.', 1, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 5);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 1);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 3);

-- przepis 10
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (2.0, 'frytki.jpg', '1h');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-08-05', NULL, 'Frytki', @sc_id, @user1ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 kg ziemniaków', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 litr oliwy lub oleju roślinnego do smażenia np. z orzeszków ziemnych', 1, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Ziemniaki obrać, pokroić na cienkie słupki. Opłukać i dokładnie osuszyć na ręcznikach..', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Jeśli ziemniaki nie będą od razu smażone, najlepiej przetrzymać je w garnku z zimną i osoloną wodą. Przed smażeniem dokładnie osuszyć..', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Podgrzać cały olej do 110 stopni C w średnim garnku. Włożyć ziemniaki i smażyć w tej samej temperaturze (można sprawdzać termometrem lub ustawić odpowiednio frytkownicę) przez około 7 - 8 minut (u mnie ogień pod garnkiem był minimalny na największym palniku). Następnie wyjąć frytki łyżką cedzakową i wyłożyć na 2 większe talerze. Odstawić olej z ognia do czasu ostudzenia ziemniaków.', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Rozgrzać ponownie tłuszcz, tym razem do 175 stopni C.', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Włożyć przestudzone frytki i smażyć przez około 5 minut do czasu aż ziemniaki będą lekko zrumienione (cały czas utrzymywać tę samą temperaturę). W międzyczasie można 1 - 2 razy ziemniaki przemieszać. Wyjąć łyżką cedzakową i ułożyć na ręcznikach papierowych. Od razu podawać posypując solą morską, najlepiej gruboziarnistą.', 4, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 1);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 2);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 3);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 4);

-- przepis 11
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (1.0, 'zupa_szparagowa_ziemniaki_feta_01.jpg', '1.30h');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-08-05', NULL, 'Zupa szparagowa', @sc_id, @user1ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka oliwy', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 cebula', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 por', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('600 g ziemniaków', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 marchewka', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka masła', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 i 1/2 litra bulionu', 6, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 listek laurowy i ziele angielskie', 7, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('szczypta kurkumy', 8, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 pęczek szparagów', 9, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('100 g serka topionego (śmietankowego)', 10, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/3 szklanki śmietanki 30% lub 18% do zup', 11, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('szczypiorek oraz koperek', 12, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('W większym garnku na oliwie zeszklić pokrojoną w kosteczkę cebulę.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Odkroić zielone liście pora. Białą i jasnozieloną część przekroić wzdłuż na 4 części, następnie dokładnie opłukać. Pokroić w poprzek na plasterki.', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Pora dodać do cebuli i podsmażyć co chwilę mieszając przez około 3 minuty.', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Dodać obraną i startą marchewkę i mieszając podsmażać ok. 2 minuty.', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Dodać masło oraz obrane i pokrojone w kosteczkę ziemniaki i znów smażyć 2 minuty co chwilę mieszając.', 4, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wlać gorący bulion, dodać listek laurowy i ziele angielskie oraz szczyptę kurkumy, zagotować. Przykryć i gotować przez ok. 15 minut aż warzywa będą miękkie.', 5, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('W międzyczasie przygotować szparagi: odłamać twarde i jasne końce (same złamią się w odpowiednim miejscu). Zielone łodyżki umyć a następnie pokroić na plasterki. Odłamane jasne końce nie będą wykorzystane (są łykowate i niesmaczne).', 6, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Szparagi dodać do zupy, zmniejszyć ogień i gotować przez ok. 1 - 2 minuty.', 7, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Trzymając zupę na minimalnym ogniu dodać pokrojony ser topiony i mieszać aż się rozpuści. Zaprawić zupę śmietanką, posypać szczypiorkiem oraz koperkiem. Podawać z pieczywem.', 8, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 2);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 4);

-- przepis 12
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (10.0, 'gotowy-pistacjowy-sernik-mascarpone.jpg', '2.30h');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-08-05', NULL, 'Sernik pistacjowy', @sc_id, @user1ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('150 g herbatników kakaowych', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('50 g masła', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('750 g zmielonego twarogu sernikowego', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('120 g (1/2 szklanki) cukru', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka mąki ziemniaczanej', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka masła', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('3 jajka', 6, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('200 g słodkiej pasty pistacjowej*', 7, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('125 ml śmietanki 18%', 8, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Twaróg i jajka wyjąć wcześniej z lodówki i ocieplić w temperaturze pokojowej.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Dno tortownicy z odpinaną obręczą o średnicy 19 (maks. 21 cm) wyłożyć papierem do pieczenia. Zapiąć obręcz wypuszczając papier na zewnątrz. ', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Herbatniki pokruszyć i połączyć z miękkim masłem (np. w rozdrabniaczu). Powstałą masą wyłożyć spód i boki tortownicy (do połowy).', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Piekarnik nagrzać do 160 stopni C (góra i dół bez termoobiegu). Na dno piekarnika wstawić keksówkę napełnioną do połowy wrzącą wodą (zapobiega to pękaniu sernika i nadaje mu odpowiedniej konsystencji).', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Do misy miksera włożyć twaróg, cukier i mąkę. Miksować na małych obrotach, aż masa będzie gładka i jednolita przez około 2 minuty.', 4, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Dodawać po jednym jajku, miksując przez około 30 sekund po każdym dodanym jajku.', 5, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('W oddzielnej misce połączyć pastę pistacjową ze śmietanką. Dodać do masy serowej i wolno zmiksować do połączenia się składników. ', 6, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Masę wylać do przygotowanej formy, wstawić do piekarnika i piec przez 70 minut (lub krócej 65 min jeśli mamy formę większą niż 19 cm). UWAGA: po upieczeniu środek sernika ma pozostać luźny.', 7, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Gotowy sernik wyjąć z piekarnika i ostudzić. Po ostudzeniu wstawić do lodówki na kilka godzin bez przykrycia.', 8, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Przed podaniem udekorować przez szprycę bitą śmietaną (ubić zimną kremówkę dodając w trakcie cukier puder) i siekanymi niesolonymi pistacjami.', 9, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 5);

-- przepis 13
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (10.0, 'p_serowe.jpg', '2.30h');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-08-05', NULL, 'Pączki serowe', @sc_id, @user1ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('250 g twarogu (tłustego)', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('100 g gęstej śmietany 12% lub 18% (w kubeczku, homogenizowanej)', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 żółtka', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/3 szklanki cukru', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka cukru wanilinowego', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('250 g mąki pszennej', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 i 1/2 łyżeczki sody oczyszczonej', 6, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 litr oleju roślinnego', 7, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('cukier puder', 8, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Do miski włożyć twaróg, śmietanę, żółtka, cukier oraz cukier wanilinowy. Rozgnieść składniki widelcem.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Dodać mąkę, sodę i wymieszać. Wyłożyć na podsypaną mąką stolnicę i połączyć składniki.', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Ciasto podzielić na ok. 28 kawałków i ulepić z nich kulki wielkości orzecha włoskiego (aby ciasto się mniej kleiło można formować umytymi i wilgotnymi dłońmi lub delikatnie oprószonymi mąką).', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Do szerokiego, dużego garnka wlać olej i rozgrzać go do około 180 stopni C. Włożyć połowę kulek (lub tyle ile się zmieści w jednej warstwie z zachowaniem małych odstępów).', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Smażyć na złoty kolor po około 1,5 minuty z każdej strony (pączki mogą się same się obracać po usmażeniu z jednej strony).', 4, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wyłowić łyżką cedzakową i ułożyć na ręcznikach papierowych. W tym samym oleju usmażyć kolejną porcję pączków.', 5, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Posypać cukrem pudrem.', 0, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 5);

-- przepis 14
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (10.0, 'placki_kuk.jpg', '20min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-08-05', NULL, 'Placki kukurydzniane', @sc_id, @user1ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('200 g kukurydzy (1/2 puszki)', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 jajko', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('3 łyżki mąki (pszennej lub w wersji b/g kukurydzianej)', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/2 łyżeczki proszku do pieczenia', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka śmietanki kremówki lub mleka', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('sól, 1/3 łyżeczki kurkumy, 1/3 łyżeczki papryki słodkiej w proszku', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('kilka listków bazylii', 6, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('olej (np. kokosowy lub inny)', 7, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Kukurydzę odcedzić i rozmiksować z dodatkiem kurkumy i papryki w proszku w rozdrabniaczu lub blenderem (na papkę z kawałkami kukurydzy mniejszej i większej wielkości).', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Przełożyć do miski, dodać posiekaną bazylię, mleko oraz jajko i wymieszać. Dodać mąkę z proszkiem do pieczenia, doprawić solą i delikatnie wymieszać na jednolitą masę.', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Rozgrzać patelnię, posmarować ją olejem, nałożyć po ok. 1 i 1/2 łyżki masy na 1 placka i delikatnie rozprowadzić na grubość ok. 7 mm. Smażyć bez przewracania przez ok. 1,5 minuty aż będą zrumienione od spodu i urosną. Wówczas przewrócić na drugą stronę i powtórzyć smażenie.', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Zdjąć z patelni i powtórzyć z koleją porcją placków. Na lepiej rozgrzanej patelni placki mogą się szybciej usmażyć.', 3, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 1);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 3);

-- przepis 15
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (10.0, 'placki_kuk.jpg', '20min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-08-05', NULL, 'Placki kukurydzniane', @sc_id, @user2ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('200 g kukurydzy (1/2 puszki)', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 jajko', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('3 łyżki mąki (pszennej lub w wersji b/g kukurydzianej)', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/2 łyżeczki proszku do pieczenia', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka śmietanki kremówki lub mleka', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('sól, 1/3 łyżeczki kurkumy, 1/3 łyżeczki papryki słodkiej w proszku', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('kilka listków bazylii', 6, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('olej (np. kokosowy lub inny)', 7, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Kukurydzę odcedzić i rozmiksować z dodatkiem kurkumy i papryki w proszku w rozdrabniaczu lub blenderem (na papkę z kawałkami kukurydzy mniejszej i większej wielkości).', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Przełożyć do miski, dodać posiekaną bazylię, mleko oraz jajko i wymieszać. Dodać mąkę z proszkiem do pieczenia, doprawić solą i delikatnie wymieszać na jednolitą masę.', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Rozgrzać patelnię, posmarować ją olejem, nałożyć po ok. 1 i 1/2 łyżki masy na 1 placka i delikatnie rozprowadzić na grubość ok. 7 mm. Smażyć bez przewracania przez ok. 1,5 minuty aż będą zrumienione od spodu i urosną. Wówczas przewrócić na drugą stronę i powtórzyć smażenie.', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Zdjąć z patelni i powtórzyć z koleją porcją placków. Na lepiej rozgrzanej patelni placki mogą się szybciej usmażyć.', 3, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 1);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 3);

-- przepis 16
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (10.0, 'jaja_r.jpg', '50min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-09-05', NULL, 'Jajka po ranczersku', @sc_id, @user2ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('80 g boczku parzonego / wędzonego', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/2 małej cebuli', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('150 g czerwonej fasoli z zalewą', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('przyprawy: sól i pieprz oraz (opcjonalnie) szczypta kminu rzymskiego i wędzonej papryki', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('tortilla (1 duża lub 2 małe)', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 łyżeczki masła', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 jajka', 6, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 pomidor lub 10 pomidorków koktajlowych', 7, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/4 czerwonej cebuli', 8, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/2 awokado', 9, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Boczek pokroić w kostkę, włożyć na patelnię i na małym ogniu wytopić z tłuszczu. Przełożyć na talerz zostawiając tłuszcz na patelni.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Na pozostałym tłuszczu zeszklić drobno posiekaną cebulę. Dodać fasolę z kilkoma łyżkami zalewy oraz przyprawy. Chwilę podgrzać po czym rozgnieść wszystko praską do ziemniaków na w miarę jednorodną pastę.', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Na dużej patelni rozgrzać łyżeczkę masła i podsmażyć placek tortilli. Tortillę przełożyć na talerz, następnie na patelnię dodać drugą łyżeczkę masła i usmażyć sadzone jajka', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('W międzyczasie przygotować salsę pomidorową: pomidora pokroić w kostkę, czerwoną cebulę drobno posiekać.', 4, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Tortillę posmarować pastą z fasoli, ułożyć sadzone jajka, podsmażony boczek i salsę pomidorową. Dodać pokrojone plasterki awokado, posypać kolendrą, doprawić solą i pieprzem. Awokado i salsę skropić sokiem z limonki, opcjonalnie dodać plasterki chili lub jalapeno.', 5, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 3);

-- przepis 17
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (10.0, 'Pan_con_tomate.jpg', '20h');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-09-05', NULL, 'Pan con tomate', @sc_id, @user2ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('pieczywo, np. bagietka, biały chleb', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('"mięsiste" pomidory, np. malinowe lub bawole serce', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('oliwa extra vergine', 2, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Pieczywo pokroić na kromki i opiec do zrumienienia, np. w tosterze lub w piekarniku pod grillem.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Pomidory zetrzeć na tarce o grubych oczkach (pomijając skórkę).', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Opieczone kromki pieczywa (opcjonalnie) przetrzeć przekrojonych ząbkiem czosnku, następnie skropić oliwą extra vergine. Nałożyć starte pomidory i posypać solą, od razu podawać.', 3, @sc_id);

-- -- tagi
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 3);

-- przepis 18
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (2.0, 'ryba_szpinak.jpg', '45min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-09-05', NULL, 'Ryba ze szpinakiem', @sc_id, @user2ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('ok. 500 g filetów białej ryby np. dorsza (świeże lub mrożone)', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 łyżki mąki pszennej', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 łyżeczki przyprawy curry w proszku*', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('200 g szpinaku (mrożonego lub świeżego)', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 łyżka masła', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 ząbek czosnku', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('125 ml śmietanki 30%', 6, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1/2 łyżeczki mąki ziemniaczanej', 7, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('3 łyżki oliwy', 8, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Filety ryby rozmrozić jeśli były mrożone. Dokładnie osuszyć papierowymi ręcznikami, doprawić solą, pieprzem, następnie obtoczyć w mieszance mąki pszennej i przyprawy curry.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Szpinak rozmrozić jeśli był mrożony. Świeży szpinak umyć, włożyć do dużego garnka i mieszając podgrzewać przez ok. 2 minuty aż zwiędnie. Wyłożyć na sitko i delikatnie odcisnąć. Pokroić na mniejsze kawałki.', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Do tego samego garnka włożyć masło, dodać starty czosnek i chwilę podsmażyć. Dodać szpinak i wymieszać. Smażyć przez 2 minuty, następnie wlać śmietankę, doprawić solą oraz pieprzem i zagotować. Oprószyć mąką ziemniaczaną, wymieszać, zagotować, odstawić z ognia.', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Rozgrzać większą patelnię z 3 łyżkami oliwy, włożyć filety i smażyć przez ok. 3 - 4 minuty w zależności od grubości filetów. Przewrócić łopatką na drugą stronę i powtórzyć smażenie.', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Obłożyć rybę szpinakiem i razem podgrzać na patelni. ', 4, @sc_id);

-- -- tags
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 2);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 4);

-- przepis 19
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (2.0, 'ryba_wegierska.png', '60min');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-09-05', NULL, 'Ryba po węgiersku', @sc_id, @user2ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('500 g filetów białej ryby np. mintaja, morszczuka, dorsza', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 jajko', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('ok. 1/2 szklanki mąki', 2, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('olej roślinny', 3, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 większa marchewka', 4, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 mała pietruszka', 5, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 czerwone papryki', 6, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('przyprawy: 2 łyżeczki słodkiej papryki, 1 łyżeczka majeranku, po 1/3 łyżeczki ostrej papryki i kminku', 7, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 szklanka bulionu', 8, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('listek laurowy, ziele angielskie', 9, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('1 szklanka przecieru pomidorowego', 10, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('natka pietruszki', 11, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Filety osuszyć, pokroić na mniejsze kawałki, doprawić solą i pieprzem. Obtoczyć w roztrzepanym jajku, następnie w mące.', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Rozgrzać olej na patelni i usmażyć rybę na złoty kolor. Odłożyć na talerz.', 1, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Umyć patelnię, wlać olej i zeszklić pokrojoną w piórka lub kostkę cebulę. Następnie dodać obraną i startą marchewkę oraz pietruszkę. Mieszając smażyć przez ok. 5 minut.', 2, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Dodać pokrojoną na paski paprykę i co chwilę mieszając smażyć przez ok. 3 minuty. Dodać przyprawy, doprawić solą i pieprzem, wymieszać, chwilę podsmażyć.', 3, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Wlać bulion, dodać listek laurowy i ziele angielskie, przykryć i gotować przez ok. 7 minut.', 4, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Dodać przecier pomidorowy, wymieszać i gotować przez ok. 3 minuty.', 5, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Sos i kawałki ryby układać w półmisku warstwami. Posypać natką pietruszki, podawać na ciepło lub zimno.', 6, @sc_id);

-- -- tags
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 2);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 4);

-- przepis 20
-- -- recipe body
INSERT INTO recipe_body(serving_quantity, photo, time_description) VALUES (3.0, 'wolowina_pieczary.jpg', '2h');
select @sc_id := scope_identity();
INSERT INTO recipe(created_on, last_modified, `name`, body_id, creator_id) VALUES ('2021-08-01', NULL, 'Wołowina w pieczarkach', @sc_id, @user2ID);
select @recipe_id := scope_identity();

-- -- składniki
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2 Pieczarki', 0, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('2kg Wołowiny', 1, @sc_id);
INSERT INTO ingredients(`data`, order_number, body_recipe_id) VALUES ('500ml wody', 2, @sc_id);

-- -- kroki
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Umyj pieczarki i zalej wodą', 0, @sc_id);
INSERT INTO steps(`data`, order_number, body_recipe_id) VALUES ('Podsmaż wołowinę', 1, @sc_id);

-- -- tagi;
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 1);
INSERT INTO recipies_tags(recipe_id, tag_id) VALUES (@recipe_id, 3);

