# backend

#### [Swagger url](http://localhost:8081/swagger-ui/index.html#/)
#### [h2 console](http://localhost:8081/h2)

## <span style="color:red">WAŻNE</span>
#### Nie wrzucamy zmian bezpośrednio na mastera (chyba, że są to małe zmiany np. zapomniałem gdzieś kropki itp)
#### <span style="color:red">( najprawdopodobniej ta opcja będzie zablokowana )</span> 

Dla każdego taska tworzymy nowy branch według wzoru: __feature/task_jira_code__ lub __feature/task_jira_code-short_description__  
Gdy zadanie zostanie skończone robimy __merge request__ i przesuwamy w jira status na verification i szukamy osoby chętnej do zrobienia weryfikacji/code review  
Kod po akceptacji wrzucamy na mastera (Należy pamiętać aby przed tą operacją podbić wersję projektu)
